from django.shortcuts import render
from .models import Service


def index(request):
    service = Service.objects.all()
    return render(request, 'shrek/index.html', {'service': service})