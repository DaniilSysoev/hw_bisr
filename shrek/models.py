from django.db import models


class Service(models.Model):
    name = models.CharField(max_length=200)
    text = models.TextField()
    cost = models.CharField(max_length=200)
    number = models.CharField(max_length=2)

    def __str__(self) -> str:
        return self.name