﻿window.addEventListener('DOMContentLoaded', () => {
    document.addEventListener('mouseover', (event) => {
        if (event.target == document.querySelector('.portfolio__navigation')) {
            const div = document.querySelector('.portfolio__navigation')
            div.addEventListener('click', (event) => {
                const buttons = div.querySelectorAll('button')
                for (var i = 0; i < buttons.length; i++) {
                    if (event.target == buttons[i]) {
                        buttons[i].classList.remove(buttons[i].className)
                        buttons[i].classList.add('portfolio__navigation__button_black')
                        const div = document.querySelector('.portfolio__example')
                        const imgs = div.querySelectorAll('img')
                        if (buttons[i].id == "all") {
                            imgs[0].style= 'display: unset'
                            imgs[1].style= 'display: unset'
                            imgs[2].style= 'display: unset'
                            imgs[3].style= 'display: unset'
                            imgs[4].style= 'display: unset'
                            imgs[5].style= 'display: unset'
                            imgs[0].classList.remove(imgs[0].className)
                            imgs[0].classList.add('portfolio__example__big')
                            imgs[1].classList.remove(imgs[1].className)
                            imgs[1].classList.add('portfolio__example__small')
                            imgs[2].classList.remove(imgs[2].className)
                            imgs[2].classList.add('portfolio__example__small')
                            imgs[3].classList.remove(imgs[3].className)
                            imgs[3].classList.add('portfolio__example__big')
                            imgs[4].classList.remove(imgs[4].className)
                            imgs[4].classList.add('portfolio__example__small')
                            imgs[5].classList.remove(imgs[5].className)
                            imgs[5].classList.add('portfolio__example__small')
                        } else {
                            for (var j = 0; j < imgs.length; j++){
                                if (imgs[j].id != buttons[i].id) {
                                    imgs[j].style='display: none'
                                }else {
                                    imgs[j].style.removeProperty('display')
                                    imgs[j].classList.remove(imgs[j].className)
                                    imgs[j].classList.add('portfolio__example__big')
                                }
                            }
                        }
                    } else {
                        buttons[i].classList.remove(buttons[i].className)
                        buttons[i].classList.add('portfolio__navigation__button_white')
                    }
                }
            })
        }
        if (event.target == document.querySelector('.cost__block_form__button')){
            const button = document.querySelector('.cost__block_form__button')
            button.addEventListener('click', () => {
                document.querySelector('.cost__block_result').style= 'display: flex'
            })
        }
        if (event.target.className == 'qa__container__big_example') {
            const button = document.getElementById(event.target.id)
            const img = button.querySelector('img')
            button.addEventListener('click', (event) => {
                const div = button.querySelectorAll('div')
                if (div[2].className == 'qa__container__big_example__main_text_close'){
                    img.style= 'transform: rotate(45deg)'
                }else {
                    img.style= 'transform: rotate(0deg)'
                }
                div[2].classList.toggle('qa__container__big_example__main_text_open')
                div[2].classList.toggle('qa__container__big_example__main_text_close')
            })
        }
    })
})